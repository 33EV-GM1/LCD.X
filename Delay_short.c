/*! \file  Delay_short.c
 *
 *  \brief Function for very short delays
 *
 *
 *  \author jjmcd
 *  \date October 5, 2015, 8:56 AM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>

/*! Delay_short() - Short delays */

/*! Delay_short() wastes a small amount of time.
 *  \param n int - loop count
 *  \return none
 */
void Delay_short(int n)
{
  int i;
  
  for ( i=0; i<n; i++ )
    Nop();

}
