/*! \file  LCDcommand.c
 *
 *  \brief Send a command to the LCD
 *
 *
 *  \author jjmcd
 *  \date January 28, 2015, 10:53 AM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "LCDinternal.h"
#include "LCD.h"

//! Send a command to the LCD
/*!  This routine simply sends a data byte to the LCD.  The
 *   register select pin is set to 0 notifying the LCD that the
 *   byte is to be interpreted as a command.
 *
 * Pseudocode:
 * \code
 * Wait for LCD
 * Set register select to zero
 * Send the byte
 * Wait 1ms
 * Set register select to one
 * \endcode
 * \callgraph
 * \callergraph
 *
 * \param cmd char - Command byte to send to LCD
 * \returns none
 */
void LCDcommand( char cmd )
{
    LCDbusy();
    LCD_RS = 0; // assert register select to 0
    Delay_ms(DELAY_VALUE_1); // fast processor?
    LCDsend( cmd );
    Delay_ms(DELAY_VALUE_1);
    LCD_RS = 1; // negate register select to 1
}

